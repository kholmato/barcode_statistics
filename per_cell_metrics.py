#! /usr/bin/env python

# all the lines starting with `#` are comments except the first one
# `#!` is called `shebang`. It tells you where the Python interpreter
# is located. On windows you can probably ignore it.

# packages needed
# can be installed with `pip install [package]`
import pysam as ps
import gzip as gz
import pandas as pd
import click
from multiprocessing import Pool
import multiprocessing
from functools import reduce
from sinto import utils


def count_flags(intervals, bam, barcode_list, tag_type):
    pbtso = {
        # define the keys (i) to be barcodes (see below) and values to be dictionaries.
        # Internal dicionaries have keys "total_umi", "tso", "polyA" and values are empty
        # sets that will be filled with UMIs from the BAM file later
        i: {"total_umi": set(), "tso": set(), "polyA": set(), "tso&polyA": set()}
        # get the keys from the barcodes file
        for i in barcode_list
    }

    # open the BAM file
    with ps.AlignmentFile(
        bam,
        "rb",
    ) as samfile:
        for i in intervals:
            # start reading the file line-by-line
            for read in samfile.fetch(i[0], i[1], i[2]):
                # skip the unmapped reads and the ones without CellBarcode or UmiBarcode
                if read.is_unmapped or not read.has_tag("CB") or not read.has_tag("UB"):
                    continue
                else:
                    cell_barcode = read.get_tag("CB")

                    # check that `cell_barcode`'s are already keys in pbtso
                    # most will not be, because they didn't pass QCs and weren't called as cells
                    if cell_barcode in pbtso:
                        # extract UmiBarcode from read
                        umi = read.get_tag("UB")
                        # add the UMI to the "total_umi" set in a corresponding dict
                        pbtso[cell_barcode]["total_umi"].add(umi)

                        if tag_type == "cellranger":
                            if read.has_tag("ts"):
                                # add the UMI to "tso" set if the read has this TAG
                                pbtso[cell_barcode]["tso"].add(umi)

                            if read.has_tag("pa"):
                                # add the UMI to "polyA" set if the read has this TAG
                                pbtso[cell_barcode]["polyA"].add(umi)

                            if read.has_tag("ts") and read.has_tag("pa"):
                                # add the UMI to "polyA" set if the read has this TAG
                                pbtso[cell_barcode]["tso&polyA"].add(umi)
                        elif tag_type == "starsolo":
                            cn_tag = read.get_tag("cN")
                            if cn_tag[0] > 0:
                                # add the UMI to "tso" set if the read has this TAG
                                pbtso[cell_barcode]["tso"].add(umi)

                            if cn_tag[1] > 0:
                                # add the UMI to "polyA" set if the read has this TAG
                                pbtso[cell_barcode]["polyA"].add(umi)

                            if cn_tag[0] > 0 and cn_tag[1] > 0:
                                # add the UMI to "polyA" set if the read has this TAG
                                pbtso[cell_barcode]["tso&polyA"].add(umi)
                        else:
                            raise ValueError(f"Unsupported tag-type: {tag_type}")
    return pbtso


@click.command()
@click.option(
    "--barcodes",
    "-bc",
    default="./1cellranger/outs/filtered_feature_bc_matrix/barcodes.tsv.gz",
    help="Path to the barcodes file.",
)
@click.option(
    "--bam",
    "-b",
    default="./test_data/small.bam",
    help="Path to the bam file",
)
@click.option(
    "--output",
    "-o",
    default="per_cell_tso_test.tsv.gz",
    help="Path to output tsv.gz file",
)
@click.option("--threads", "-t", default=1, help="Number of threads to use")
@click.option(
    "--tag-type",
    "-k",
    type=click.Choice(["cellranger", "starsolo"], case_sensitive=False),
    default="cellranger",
    help="Number of threads to use",
)
def main(barcodes, bam, output, threads, tag_type):
    with ps.AlignmentFile(
        bam,
        "rb",
    ) as samfile:
        # chroms = samfile.references
        intervals = utils.chunk_bam(samfile, threads)

    # read the barcodes file as a pandas dtaframe and extract the barcodes column
    barcode_list = pd.read_csv(
        barcodes,
        header=0,
        names=["Barcode"],
    )["Barcode"]

    with Pool(processes=threads) as pool:
        results = pool.starmap_async(
            count_flags,
            [(intervals[i], bam, barcode_list, tag_type) for i in intervals],
        )
        pbtso_all_chrom = results.get()

    pbtso = {
        i: {
            "total_umi": set().union(*[x[i]["total_umi"] for x in pbtso_all_chrom]),
            "tso": set().union(*[x[i]["tso"] for x in pbtso_all_chrom]),
            "polyA": set().union(*[x[i]["polyA"] for x in pbtso_all_chrom]),
            "tso&polyA": set().union(*[x[i]["tso&polyA"] for x in pbtso_all_chrom]),
        }
        for i in barcode_list
    }

    with gz.open(output, "wt") as res:
        res.write("Barcode\ttotal_umi\ttso\tpolyA\ttso_and_polyA\n")
        res.write(
            # write lines separated by "\n" - standard line separator
            "\n".join(
                [
                    # write tab separated line
                    "\t".join(
                        [
                            key,  # key of the pbtso - Cell Barcode
                            str(
                                len(value["total_umi"])
                            ),  # length of the "total_umi" set converted to a string
                            str(
                                len(value["tso"])
                            ),  # length of the "tso" set converted to a string
                            str(
                                len(value["polyA"])
                            ),  # length of the "polyA" set converted to a string
                            str(
                                len(value["tso&polyA"])
                            ),  # length of the "tso&polyA" set converted to a string
                        ]  # this is a list literal syntax
                    )
                    for (key, value) in pbtso.items()
                ]  # this is a list comprehension syntax
            )
        )


if __name__ == "__main__":
    main()
